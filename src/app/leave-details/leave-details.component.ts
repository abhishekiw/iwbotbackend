import { Component, OnInit, OnDestroy } from '@angular/core';
import { LeaveService } from '../_services/leave.service';
import { NgForm } from '@angular/forms';
import { Status, Leave } from '../_models/leave';
import { ToastrService } from 'ngx-toastr';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-leave-details',
  templateUrl: './leave-details.component.html',
  styleUrls: ['./leave-details.component.scss']
})
export class LeaveDetailsComponent implements OnDestroy, OnInit {
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<Leave> = new Subject();

  public details: Leave[];
  status: any = {
    ID: "",
    Status: "",
    empName: "",
    leaveDate: "",
    leaveDuration: "",
    leaveType: "",
    leaveReason: ""
  };
  constructor(private leaveService: LeaveService, private toastr: ToastrService) { }

 
  StatusList: string[] = ["Approved", "Pending", "Denied"]
  ngOnInit(): void {
    // ====get leave details====
    this.getLeave();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }


  // =============== getting leavesetails=========================
  getLeave() {
    this.leaveService.getLeaveDetails()
      .subscribe((data: any) => {
        this.details = data;
        this.dtTrigger.next();
        // console.log(this.details);

      });
  }

  // ===============add status name======================
  // OnSubmit(form: NgForm) {
  //   // console.log(this.status);
  //   if (this.details.ID !== null) {
  //     this.leaveService.addStatus(this.status)
  //       .subscribe((data: Status) => {
  //         // console.log(data);
  //         this.getLeave();
  //         this.toastr.warning('Status successfully added!');
  //       },
  //         error => (this.toastr.error("Status not added!"))
  //       )
  //   }

  // }



  changeStatus(val: any, details: any) {
    this.status.ID = details.id;
    this.status.Status = val;
    this.status.EmpName = details.empName;
    this.status.LeaveDate = details.leaveDate;
    this.status.LeaveDuration = details.leaveDuration;
    this.status.LeaveReason = details.leaveReason;
    this.status.LeaveType = details.leaveType;
    // console.log(this.status);
    // debugger
    this.leaveService.addStatus(this.status).subscribe((data: any) => {
      this.status = data
      this.toastr.warning('Status successfully added!');
    },
      error => (this.toastr.error("Status not added!"))
  );
    location.reload();
  }
}
