import { Component, OnInit } from '@angular/core';
import { FoodsService } from '../_services/foods.service';
import { NgForm } from '../../../node_modules/@angular/forms';
import { Orders } from '../_models/order';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { OrderService } from '../_services/order.service';

@Component({
  selector: 'app-order-lunch',
  templateUrl: './order-lunch.component.html',
  styleUrls: ['./order-lunch.component.scss']
})
export class OrderLunchComponent implements OnInit {

  constructor(private foodsService: FoodsService, private orderService: OrderService, private toastr: ToastrService) { }
  public foodDetails: any;

  order: any = {
    FoodID: "",
    Quantity: "",
    Description: ""
  };

  ngOnInit() {
    this.getFoodDetails();
    this.resetForm();

  }

  // =============reset form=================

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.order = {
      FoodID: "",
      Quantity: "",
      Description: ""
    }
  }


  // =====================get food category names ===================
  getFoodDetails() {
    this.foodsService.getFoodDetails()
      .subscribe((data: any) => {
        this.foodDetails = data;
        // console.log(this.categoryDetails);
      });
  }


  // ===============add food name======================
  OnSubmit(FoodID, Description, Quantity) {
    this.orderService.addOrder(FoodID, Description, Quantity)
      .subscribe((data: Orders) => {
        this.resetForm();
        this.toastr.success('Order successfully added!', "Lunch Order");
      },
        error => (this.toastr.error("Order not added!", "Lunch Order"))
      )
  }
}
