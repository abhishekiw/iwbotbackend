export class Employee {
    ID: string;
    Email: string;
    DOB: Date;
    Gender: number;
    CreatedDate: Date;
}

export interface IEmployee {
    ID: string;
    Email: string;
    DOB: Date;
    Gender: number;
    CreatedDate: Date;
}
