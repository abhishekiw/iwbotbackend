export interface Notification {
    ID: string;
    Message: string;
    CreatedDate: Date;
}

export class Notifications{
    Message: string;
}