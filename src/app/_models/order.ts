export interface IOrder {
    ID: string;
    EmpName: string;
    FoodName: string;
    Quantity: number;
    Description: string;
}


export class Orders{
    FoodID: string;
    Quantity: number;
    Description: string;
}