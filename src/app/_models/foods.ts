export interface IFoodCategory {
    ID: string;
    Name: string;
}
export class FoodCategory{
    Name: string;
}
export interface IFoods {
    ID: string;
    FoodCategoryName: string;
    Name: string;
    Price: number;
}

export class Foods{
    FoodCategoryID: string;
    Name: string;
    Price: number;
}
