export interface Leave {
    ID: string;
    EmpName: string;
    EmpId: string;
    LeaveDate: Date;
    LeaveDuration: string;
    LeaveType: string;
    LeaveReason: string;
    Status: number;
}

export class Status{
    ID: string;
    Status: number;
    EmpName: string;
    LeaveDate: Date;
    LeaveDuration: string;
    LeaveType: string;
    LeaveReason: string;
}

export class AskLeave{
    UserId: string;
    LeaveDate: Date;
    LeaveDuration: string;
    LeaveType: string;
    LeaveReason: string;
}