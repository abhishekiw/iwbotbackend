import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoginService } from '../_services/login.service';
import { first } from 'rxjs/operators';
import { ToastrService } from "ngx-toastr";
import { debug } from 'util';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, 
    private loginService: LoginService, private toastr: ToastrService) { }

 myData;
  ngOnInit() {
    this.checkLoggedIn();
  }

  OnSubmit(Email, Password){
    // console.log(Email, Password);
    this.loginService.userAuthentication(Email, Password)
    .subscribe(
      data => {
        // console.log(data);
        this.myData = data;
        if (this.myData["code"] == '200') {
          localStorage.setItem('userToken', JSON.stringify({ Email, token: this.myData["data"], role: this.myData["message"] }));
        }
        var userRoles= JSON.parse(localStorage.getItem('userToken'));
        // console.log(userRoles.role);
        if(userRoles.role == 'Admin'){
          this.router.navigate(['/top-nav/dashboard']);
        }
        else if(userRoles.role == 'Employee'){
        this.router.navigate(['/main-nav/OrderLunch']);          
        }
        //location.reload();
      },
      error => (this.toastr.error('Please Check your Email and Password!', 'Login')));
    }
    
  //   .subscribe((data: any) => {
  //     // console.log("hello");
  //     localStorage.setItem('userToken', data.auth_token);
  //     this.router.navigate(['/top-nav/dashboard']);
  //   },
  //   error => (this.toastr.error('Please Check your Email and Password!', 'Login')));
  // }


  checkLoggedIn(){
    this.loginService.checkLoggedIn();
  }
 

} 

