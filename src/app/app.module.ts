import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import {NgxPaginationModule} from 'ngx-pagination';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
//for mdbootstrap
import { NgModule, NO_ERRORS_SCHEMA  } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ToastrModule } from "ngx-toastr";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
//components
import { TopNavComponent } from './top-nav/top-nav.component';
import { EmpDetailsComponent } from './emp-details/emp-details.component';
import { NotificationComponent } from './notification/notification.component';
import { LeaveDetailsComponent } from './leave-details/leave-details.component';
import { FoodDetailsComponent } from './food-details/food-details.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EmployeesService } from './_services/employees.service';
import { FoodsService } from './_services/foods.service';
import { NotificationService } from './_services/notification.service';
import { LeaveService } from './_services/leave.service';
import { OrderService } from './_services/order.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DashboardService } from './_services/dashboard.service';
import { LoginService } from './_services/login.service';
import { OrderLunchComponent } from './order-lunch/order-lunch.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LeaveApplicationComponent } from './leave-application/leave-application.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    TopNavComponent,
    EmpDetailsComponent,
    NotificationComponent,
    LeaveDetailsComponent,
    FoodDetailsComponent,
    OrderDetailsComponent,
    LoginComponent,
    DashboardComponent,
    PageNotFoundComponent,
    OrderLunchComponent,
    MainNavComponent,
    LeaveApplicationComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    MDBBootstrapModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxChartsModule,
    NgbModule.forRoot(),
    DataTablesModule,    
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
  })
    
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },
    EmployeesService, FoodsService, NotificationService,
     LeaveService,OrderService, DashboardService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
