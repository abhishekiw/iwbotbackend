import { Component, OnInit } from '@angular/core';
import { LeaveService } from '../_services/leave.service';
import { DashboardService } from '../_services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private leaveService: LeaveService, private dashboardService: DashboardService) { }
  public details = [];
  public orderCounts;
  public empCounts;
  public leaveCounts;
  ngOnInit() {
    this.getLeave();
    this.getEmpCount();
    this.getLeaveCount();
    this.getOrderCount();
}
  
// ===========================================================
// ===============methods for data ===========================
// ===========================================================

// =============== getting message for table=========================
getLeave(){
    this.leaveService.getLeaveDetails()
    .subscribe((data: any)=>{
  this.details = data;
  // console.log(this.details);
    });
  }

  // =================order count ===================
  getOrderCount(){
    this.dashboardService.getTotalOrder().subscribe((data: any) => {
      this.orderCounts = data;
      // console.log(this.orderCounts);
    });
  }


  // leave count
  getLeaveCount(){
    this.dashboardService.getTotalLeave().subscribe(data => {
      this.leaveCounts = data;
    });
  }


  // emp count
  getEmpCount(){
    this.dashboardService.getTotalEmployee().subscribe(data => {
      this.empCounts = data;
    });
  }
}



