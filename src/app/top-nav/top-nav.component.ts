import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesService } from '../_services/employees.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, 
    private employeeService: EmployeesService, public app: AppComponent) { }

  ngOnInit() {
  }

  showDashboard(){
    this.router.navigate(['dashboard'], {relativeTo: this.route});
  }

  showEmpDetails(){
    this.router.navigate(['empDetails'], {relativeTo: this.route});
  }

  showNotification(){
    this.router.navigate(['Notification'], {relativeTo: this.route});
  }

  showLeaveDetails(){
    this.router.navigate(['LeaveDetails'], {relativeTo: this.route});
  }

  showFoodMenu(){
    this.router.navigate(['FoodDetails'], {relativeTo: this.route});
  }

  showOrderDetails(){
    this.router.navigate(['OrderDetails'], {relativeTo: this.route});
  }

  logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/Login'])
  }


  get email(): any{
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    return usertoken.Email;
  }
  
}
