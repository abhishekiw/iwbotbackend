import { Component, OnInit } from '@angular/core';
import { NgbDateAdapter, NgbDateStruct, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AskLeave } from '../_models/leave';
import { NgForm } from '../../../node_modules/@angular/forms';
import { LeaveService } from '../_services/leave.service';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import swal from 'sweetalert2';

@Component({
  selector: 'app-leave-application',
  templateUrl: './leave-application.component.html',
  styleUrls: ['./leave-application.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: LeaveApplicationComponent }]

})
export class LeaveApplicationComponent implements OnInit {

  constructor(config: NgbDatepickerConfig, private LeaveService: LeaveService, private toastr: ToastrService) { 
    config.minDate = { year: 1900, month: 1, day: 1 };
    config.maxDate = { year: 2099, month: 12, day: 31 };
  }


  leave: AskLeave = {
    UserId: "",
    LeaveDate: null,
    LeaveDuration: "",
    LeaveReason: "",
    LeaveType: ""
  }

  fromModel(date: Date): NgbDateStruct {
    return (date && date.getFullYear) ? { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() } : null;
  }

  toModel(date: NgbDateStruct): Date {
    return date ? new Date(date.year, date.month - 1, date.day) : null;
  }
  ngOnInit() {
    this.getNotify();
  }

  status = [];

  // ===============add food name======================
  onSubmit(form: NgForm) {
    // console.log(this.leave);
    this.LeaveService.postLeave(this.leave)
      .subscribe((data: AskLeave) => {
        this.toastr.success('Leave application successfully sended!', "Leave Application");
      },
        error => (this.toastr.error("Leave application not sended!", "Leave Application"))
      )
  }

  getNotify(){
   this.LeaveService.getLeaveDetails().subscribe((data: any) => {
     this.status = data;
     console.log(status);
   })
  }

}
