import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import { ToastrService } from '../../../node_modules/ngx-toastr';
import { LoginService } from '../_services/login.service';
import { NgbDateAdapter, NgbDateStruct, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [{ provide: NgbDateAdapter, useClass: RegisterComponent }]

})
export class RegisterComponent implements OnInit {

  constructor(config: NgbDatepickerConfig, private router: Router, private route: ActivatedRoute,
    private loginService: LoginService, private toastr: ToastrService) {
    config.minDate = { year: 1900, month: 1, day: 1 };
    config.maxDate = { year: 2099, month: 12, day: 31 };
  }

  fromModel(date: Date): NgbDateStruct {
    return (date && date.getFullYear) ? { year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate() } : null;
  }

  toModel(date: NgbDateStruct): Date {
    return date ? new Date(date.year, date.month - 1, date.day) : null;
  }

  ngOnInit() {
    this.checkLoggedIn();
  }

  OnSubmit(Email, Password, ConfirmPasssword, DOB) {
    // console.log(Email, Password, ConfirmPasssword);
    this.loginService.registerUser(Email, Password, ConfirmPasssword, DOB).subscribe((data: any) => {
      this.toastr.success('Registration successfull!', "User Registration");
      this.router.navigate(['/Login']);
    },
      error => (this.toastr.error('Registration unsuccessfull!', 'User Registration')));
  }


  checkLoggedIn(){
    this.loginService.checkLoggedIn();
  }
}
