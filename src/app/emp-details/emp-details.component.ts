import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmployeesService } from '../_services/employees.service';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs';
import { Employee } from '../_models/employee';

@Component({
  selector: 'app-emp-details',
  templateUrl: './emp-details.component.html',
  styleUrls: ['./emp-details.component.scss']
})
export class EmpDetailsComponent implements OnDestroy, OnInit {
//for datatable
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<Employee> = new Subject();


  public empDetails: Employee[];

  constructor(private employeeService: EmployeesService) { }
  ngOnInit(): void {

    // ==========get employee to show in table====================
    this.employeeService.getEmployeeDetails()
      .subscribe((data: any)=> {
      this.empDetails = data;
      this.dtTrigger.next();
      // console.log(this.empDetails);
    });


    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

}
