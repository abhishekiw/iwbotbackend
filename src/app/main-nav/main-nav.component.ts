import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  //lunch order
  showLunchOrder(){
    this.router.navigate(['OrderLunch'], {relativeTo: this.route});
  }

  // leave application
  showLeaveApplication(){
    this.router.navigate(['LeaveApplication'], {relativeTo: this.route});
  }


  logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/Login'])
  }

  
  get email(): any{
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    return usertoken.Email;
  }
}
