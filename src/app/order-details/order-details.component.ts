import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrderService } from '../_services/order.service';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs';
import { IOrder } from '../_models/order';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnDestroy, OnInit {
  // dtOptions: DataTables.Settings = {};
  dtOptions: any = {};
  dtTrigger: Subject<IOrder> = new Subject();
  constructor(private orderService: OrderService) { }
  public foodOrders = [];
  public todaysOrders = [];
  ngOnInit(): void {
    this.getOrders();
    this.getDailyOrders();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,

      dom: 'Bfrtip',
      buttons: [
        'copy'
      ]
    };

  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }



  //#region getting orders for table
  getOrders() {
    this.orderService.getOrder()
      .subscribe((data: any) => {
        this.foodOrders = data;
        this.dtTrigger.next();
        // console.log(this.foodOrders);
      });
  }
  //#endregion


  //#region getting daily orders for table
  getDailyOrders() {
    this.orderService.getDailyOrder().subscribe((data: any) => {
      this.todaysOrders = data;

    });
  }
  //#endregion

}
