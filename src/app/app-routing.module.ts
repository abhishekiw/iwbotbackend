import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpDetailsComponent } from './emp-details/emp-details.component';
import { NotificationComponent } from './notification/notification.component';
import { LeaveDetailsComponent } from './leave-details/leave-details.component';
import { FoodDetailsComponent } from './food-details/food-details.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { AuthGuard } from './_guards/auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { OrderLunchComponent } from './order-lunch/order-lunch.component';
import { LeaveApplicationComponent } from './leave-application/leave-application.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path: "", redirectTo: '/Login', pathMatch: 'full'},
  {path: "Login", component: LoginComponent},
  {path: "Register", component: RegisterComponent},
  { 
    path: "top-nav", 
    component: TopNavComponent, 
    canActivate: [AuthGuard],
    data:{roles:['Admin']},
    children: [
      { path: "empDetails", component: EmpDetailsComponent},
      { path: "Notification", component: NotificationComponent},
      { path: "LeaveDetails", component: LeaveDetailsComponent},
      { path: "FoodDetails", component: FoodDetailsComponent},
      { path: "OrderDetails", component: OrderDetailsComponent},
      {path: "dashboard", component: DashboardComponent}
    ]
  },
  { 
    path: "main-nav", 
    component: MainNavComponent,
    canActivate: [AuthGuard],
    data:{roles:['Employee']},
    children: [
      { path: "OrderLunch", component: OrderLunchComponent},
      { path: "LeaveApplication", component: LeaveApplicationComponent}
    ]
  },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
