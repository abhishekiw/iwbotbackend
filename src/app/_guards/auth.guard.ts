import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../_services/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private loginService: LoginService){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    if (localStorage.getItem('userToken') != null){
      let roles = next.data["roles"] as Array<string>;
      if (roles) {
        var match = this.loginService.roleMatch(roles);
        if (match)
          return true;
        else {
          return false;
        }
      }
      else
      return true;
    }
    else{
      this.router.navigate(['/Login']);
      return false;
    }
  }

}
