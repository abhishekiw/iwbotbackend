import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NotificationService } from '../_services/notification.service';
import { Notification, Notifications } from '../_models/notification';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Subject } from '../../../node_modules/rxjs';
import { Response } from '@angular/http';
import { DataTableDirective } from '../../../node_modules/angular-datatables';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnDestroy, OnInit {
  //for datatable
  dtOptions: any;
  dtTrigger: Subject<Notifications> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  constructor(private notificationService: NotificationService, private toastr: ToastrService) { }
  public notificationMessages: any;
  message: Notifications;

  ngAfterViewInit(): void {this.dtTrigger.next();}
  ngOnInit(): void {
    this.getNotification();
    this.resetForm();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      dom: 'Bfrtip',
      buttons: [
        'print'
      ]
    };
    
  }

  // =============== getting message for table=========================
  getNotification() {
    this.notificationService.getMessage()
      .subscribe((data: any) => {
        this.notificationMessages = data;
        // this.dtTrigger.next();
      });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }



  // =============reset form=================

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.message = {
      Message: ''
    }
  }




  //===================for deleting menu=================
  onDelete(Id: string) {
    if (confirm('Are you sure to delete this record?') == true) {
      this.notificationService.deleteMessage(Id)
        .subscribe((data: Notification) => {
          // this.dtTrigger.next();
          this.getNotification();
          // this.resetForm();
          this.toastr.warning('Notification message successfully deleted!');
        },
          error => (this.toastr.error("Notification message deleted!"))
        )
    }
  }


  //  submit message
  OnSubmit(form: NgForm) {
    console.log(this.message);
    this.notificationService.addMessage(this.message)
      .subscribe((data: Notifications) => {
        // console.log(data);
        this.getNotification();
        this.toastr.success('Notification message successfully added!');
      },
        error => this.toastr.error(error.Message))

  }
}
