import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FoodsService } from '../_services/foods.service';
import { ToastrService } from "ngx-toastr";
import { IFoods, FoodCategory, Foods } from '../_models/foods';
import { NgForm } from '@angular/forms';
import { Response } from '@angular/http';
import { Subject } from 'rxjs';
import swal from 'sweetalert2';
import { DataTableDirective } from '../../../node_modules/angular-datatables';

@Component({
  selector: 'app-food-details',
  templateUrl: './food-details.component.html',
  styleUrls: ['./food-details.component.scss']

})
export class FoodDetailsComponent implements OnDestroy, OnInit {
  dtOptions: any;
  dtTrigger: Subject<Foods> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  constructor(private foodsService: FoodsService, private toastr: ToastrService, private toastrService: ToastrService) { }

  public foodDetails = [];
  public categoryDetails: any;
  cat: any = {
    Name: ""
  };

  food: any = {
    Name: "",
    Price: ""
  };
  ngAfterViewInit(): void {this.dtTrigger.next();}
  ngOnInit(): void {

    this.getFoodData();
    this.getFoodCategoryList();
    this.resetForm();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      dom: 'Bfrtip',
      buttons: [
        'print'
      ]
    };
  }


  // =============reset form=================

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.cat = {
      Name: ''
    }
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  // ========================get food details to show in table===================
  getFoodData() {
    this.foodsService.getFoodDetails()
      .subscribe((data: any) => {
        this.foodDetails = data;
        // this.dtTrigger.next();
        // console.log(this.foodDetails);
      });
  }

  // =====================get food category names ===================
  getFoodCategoryList() {
    this.foodsService.getFoodCategory()
      .subscribe((data: any) => {
        this.categoryDetails = data;
        // console.log(this.categoryDetails);
      });
  }

  //===================for deleting menu=================
  
  // onDelete(Id: string) { 
  //   swal({
  //     title: 'Are you sure?',
  //     text: 'You will not be able to recover this imaginary file!',
  //     type: 'warning',
  //     showCancelButton: true,
  //     confirmButtonText: 'Yes, delete it!',
  //     cancelButtonText: 'No, keep it'
  //   }).then((confirm) =>{
  //     {
  //       console.log(Id);
  //       this.foodsService.deleteFood(Id)
  //         .subscribe((data: IFoods) => {
  //           this.getFoodData();
  //           // this.toastr.warning('Food item successfully deleted!');
  //         },
  //           // error => (this.toastr.error("Food item not deleted!"))
  //         )
  //     }
  //     if (confirm.value){
  //       swal("Deleted!", "Food item has been deleted.", "success");
  //     }else{
  //       swal("Cancelled", "Food item is safe :)", "error");
  //     }
  //   })
    
  // }

  

// ===========edit food menu==================
onEdit(foodDetail: any ) {
  console.log(foodDetail);
  this.food = Object.assign({}, foodDetail);
}

  // ===============add category name======================
  OnSubmit(form: NgForm) {
    // console.log(this.cat);
    this.foodsService.addCategory(this.cat)
      .subscribe((data: FoodCategory) => {
        this.resetForm();
        this.getFoodCategoryList();
        this.toastr.success('Food category successfully added!', "Food Category");
      },
        error => this.toastrService.error("Food category not added!", "Food Category")
      )
  }


  // ===============add food name======================
  OnAdding(form: NgForm) {
    // console.log(this.food);
    this.foodsService.addFood(this.food)
      .subscribe((data: Foods) => {
        // console.log(data);
        this.getFoodData();
        this.toastr.success('Food successfully added!', "Food Menu");
      },
        error => (this.toastr.error("Food not added!", "Food Menu"))
      )
  }




  

}

// $('#menu').on('show.bs.modal', function(event) {
//     var button = $(event.relatedTarget) // Button that triggered the modal
//     var recipient = button.data('whatever') // Extract info from data-* attributes
//         // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//         // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//     var modal = $(this)
//   //   modal.find('.modal-title').text('New message to ' + recipient)
//     modal.find('.modal-body input').val(recipient)
//   })
