import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private _http: HttpClient, private router: Router) { }
  //private azureUrl="https://iwbotapi.azurewebsites.net/"
  private localUrl = "http://localhost:54470/"
  userAuthentication(Email, Password) {
    var reqHeader = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._http.post(this.localUrl + 'Auth/Post',
     { email: Email, password: Password }, { headers: reqHeader }).pipe(catchError(this.errorHandler));

  }

  checkLoggedIn() {
    var userToken= JSON.parse(localStorage.getItem('userToken'));
    var token = userToken.token;
    var role = userToken.role;
    if (token != isNullOrUndefined) {
      if(role == 'Admin'){
        this.router.navigate(['/top-nav/dashboard']);
      }
      else if(userToken.role == 'Employee'){
        this.router.navigate(['/main-nav/OrderLunch']);
      }
    }
  }


  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || 'Server Error');
  }



  // =====================For registration=================
  registerUser(Email, Password, ConfirmPassword, DOB){
    return this._http.post(this.localUrl + 'Auth/Register',
     { email: Email, password: Password, confirmPassword: ConfirmPassword, dob: DOB }).pipe(catchError(this.errorHandler));
  }



  
  roleMatch(allowedRoles): boolean {
    var isMatch = false;
    var userToken= JSON.parse(localStorage.getItem('userToken'));
    var userRoles = userToken.role;
    // console.log(userRoles);
    allowedRoles.forEach(element => {
      if (userRoles.indexOf(element) > -1) {
        isMatch = true;
        return false;
      }
    });
    return isMatch;
  }
}
