import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Notification } from '../_models/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private _http: HttpClient) { }
  //private azureUrl="https://iwbotapi.azurewebsites.net/"
  private localUrl = "http://localhost:54470/"
  
  // ================getting notification message=================
  getMessage(): Observable<Notification[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.get<Notification[]>(this.localUrl + "api/Notifications", httpOptions).pipe(catchError(this.errorHandler));
  }


  // ================deleting food item from menu===================
  deleteMessage(Id: string) {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.delete(this.localUrl + 'api/Notifications/' + Id, httpOptions).pipe(catchError(this.errorHandler));
  }


  // ===================post message==================
  addMessage(message: any) {
    const body: any = {
      Message: message.Message,
    }
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.post(this.localUrl + 'api/Notifications', body, httpOptions).pipe(catchError(this.errorHandler));
  }


  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || 'Server Error');
  }

}
