import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Leave, Status, AskLeave } from '../_models/leave';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class LeaveService {

  constructor(private _http: HttpClient) { }
  //private azureUrl="https://iwbotapi.azurewebsites.net/"
  private localUrl = "http://localhost:54470/"

  // ================getting leavedetails=================
  getLeaveDetails(): Observable<Leave[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.get<Leave[]>(this.localUrl + "api/LeaveDetails", httpOptions).pipe(catchError(this.errorHandler));
  }



  // =================add status name ========================
  addStatus(status: any): Observable<Leave> {
    const body: Status = {
      ID: status.ID,
      Status: status.Status,
      EmpName: status.EmpName,
      LeaveDate: status.LeaveDate,
      LeaveDuration: status.LeaveDuration,
      LeaveType: status.LeaveType,
      LeaveReason: status.LeaveReason
    }
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.put<Leave>(this.localUrl + 'api/LeaveDetails/' + body.ID, body, httpOptions).pipe(catchError(this.errorHandler));
  }


  postLeave(leaveData: AskLeave) {
    // console.log(leaveData);
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const token = usertoken.token;
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(token);
        const isExpired = helper.isTokenExpired(token);
        const userID = decodedToken["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"];
    const body: AskLeave = {
      UserId: userID,
      LeaveDate: leaveData.LeaveDate,
      LeaveDuration: leaveData.LeaveDuration,
      LeaveReason: leaveData.LeaveReason,
      LeaveType: leaveData.LeaveType
    }
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    // console.log(usertoken.token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
      })
    };
    return this._http.post(this.localUrl + 'api/LeaveDetails', body, httpOptions).pipe(catchError(this.errorHandler));
  }


  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || 'Server Error');
  }
}
