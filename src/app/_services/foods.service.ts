import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IFoods, IFoodCategory, FoodCategory, Foods } from '../_models/foods';

@Injectable({
  providedIn: 'root'
})
export class FoodsService {

  constructor(private _http: HttpClient) { }

  //private azureUrl="https://iwbotapi.azurewebsites.net/"
  private localUrl = "http://localhost:54470/"

  // ==========geting food item details====================
  getFoodDetails(): Observable<IFoods[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    var foodUrl = this._http.get<IFoods[]>(this.localUrl + 'api/foods', httpOptions).pipe(catchError(this.errorHandler));
    return foodUrl;
  }


  // ===============get food categories list
  getFoodCategory(): Observable<IFoodCategory[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.get<IFoodCategory[]>(this.localUrl + "api/FoodCategories", httpOptions).pipe(catchError(this.errorHandler));
  }


  // ================deleting food item from menu===================
  deleteFood(Id: string) {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.delete(this.localUrl + 'api/Foods/' + Id, httpOptions).pipe(catchError(this.errorHandler));
  }


  // =================add category name ========================
  addCategory(category: any) {
    const body: FoodCategory = {
      Name: category.Name

    }
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.post(this.localUrl + 'api/FoodCategories', body, httpOptions)
    .pipe(catchError(this.errorHandler));
  }


  //=======================update category name ====================
  updateCategory(category: any) {
    const body: IFoodCategory = {
      ID: category.ID,
      Name: category.Name

    }
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.put(this.localUrl + 'api/FoodCategories/' + body.ID, body, httpOptions).pipe(catchError(this.errorHandler));
  }



  // =================add food name ========================
  addFood(food: any) {
    const body: Foods = {
      FoodCategoryID: food.FoodCategoryID,
      Name: food.Name,
      Price: food.Price
    }
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.post(this.localUrl + 'api/Foods', body, httpOptions).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || 'Server Error');
  }


}

