import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IOrder, Orders } from '../_models/order';
import { JwtHelperService } from '../../../node_modules/@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private _http: HttpClient) { }
  //private azureUrl="https://iwbotapi.azurewebsites.net/"
  private localUrl = "http://localhost:54470/"
  
  // ==========geting food order details====================
  getOrder(): Observable<IOrder[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    var foodUrl = this._http.get<IOrder[]>(this.localUrl + 'api/FoodOrders', httpOptions).pipe(catchError(this.errorHandler));
    return foodUrl;
  }


  // ==========geting daily food order details====================
  getDailyOrder(): Observable<IOrder[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.get<IOrder[]>(this.localUrl + 'api/FoodOrders/DailyOrders', httpOptions).pipe(catchError(this.errorHandler));

  }


  // =================add food order ========================
  addOrder(FoodID, Description, Quantity) {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const token = usertoken.token;
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    const isExpired = helper.isTokenExpired(token);
    const userID = decodedToken["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"];
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.post(this.localUrl + 'api/FoodOrders', {UserId: userID, FoodID: FoodID, Description: Description, Quantity: Quantity }, httpOptions).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || 'Server Error');
  }
}
