import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IDashboard } from '../_models/dashboard';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private _http: HttpClient) { }
  //private azureUrl="https://iwbotapi.azurewebsites.net/"
  private localUrl = "http://localhost:54470/"
  //total number of order count
  getTotalOrder(): Observable<IDashboard[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.get<IDashboard[]>(this.localUrl + "api/FoodOrders/totalOrder", httpOptions).pipe(catchError(this.errorHandler));
}

  //total number of employee
  getTotalEmployee(): Observable<IDashboard[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.get<IDashboard[]>(this.localUrl + "api/Employees/totalEmployee", httpOptions).pipe(catchError(this.errorHandler));
  }


  //total number of leave
  getTotalLeave(): Observable<IDashboard[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    // console.log(usertoken);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
    };
    return this._http.get<IDashboard[]>(this.localUrl + "api/LeaveDetails/totalLeave", httpOptions).pipe(catchError(this.errorHandler));
  }


  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || 'Server Error');
  }
}
