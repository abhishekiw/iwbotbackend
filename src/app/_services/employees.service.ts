import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError  } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IEmployee } from '../_models/employee';

@Injectable()
export class EmployeesService {

  constructor(private _http: HttpClient) { }
  //private azureUrl="https://iwbotapi.azurewebsites.net/"
  private localUrl = "http://localhost:54470/"

  getEmployeeDetails(): Observable<IEmployee[]> {
    var usertoken = JSON.parse(localStorage.getItem('userToken'));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + usertoken.token
        
      })
      
    };
    var employeeUrl = this._http.get<IEmployee[]>(this.localUrl + 'api/employees', httpOptions).pipe(catchError(this.errorHandler));
    return employeeUrl;
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.error || 'Server Error');
  }
} 
